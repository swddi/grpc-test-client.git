<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Swddi\Grpc\Controller;

use Google\Protobuf\Internal\Message;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\HttpServer\Router\DispatcherFactory;
use Hyperf\HttpServer\Router\Handler;
use Hyperf\Utils\Str;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Swddi\Grpc\Client\GrpcTestClient;

/**
 * @Controller(prefix="swddi/grpc", server="grpc-helper-server")
 */
class GRpcTestClientController
{
    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;


    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @RequestMapping(path="test", methods="get,post")
     */
    public function testClient()
    {
        $response = $this->response;
        $input = $this->request->all();
        if (!empty($input)) {
            [$uri, $class, $method] = explode('|', $input['route']);
            $requestProto = $input['request'];
            $responseProto = $input['response'];
            $values = $input['params'] ?? [];
            $data = $this->formatArr($values);
            //设置属性
            $requestMessage = new $requestProto();
            foreach ($data as $key => $value) {
                //获取setter方法
                $actionTrance = $this->actionTrance($key);
                //获取方法注入参数类型
                [$setterNeedType, $msg] = $this->getSetterNeedType($requestProto, $actionTrance);
                if (empty($setterNeedType)) {
                    return $response->json(['data' => $msg]);
                }
                if (!method_exists($requestMessage, $actionTrance)) {
                    return $response->json(['data' => "setter不存在！$requestMessage::$actionTrance"]);
                };
                //optional 普通类型验证
                if (in_array($setterNeedType, ['int', 'string', 'bool'])) {
                    @eval('$check=' . 'is_' . $setterNeedType . '($value);');
                    if ($check === false) {
                        return $response->json(['data' => "参数类型错误！$key"]);
                    }
                    $setterData = $value;
                } else {
                    //repeated类型数据
                    if (strpos($setterNeedType, '[]') !== false) {
                        $setterNeedType = str_replace('[]', '', $setterNeedType);
                        $setterData = [];
                        foreach ($value as $item) {
                            if (!empty($item)) {
                                //普通类型
                                if (in_array($setterNeedType, ['int', 'string'])) {
                                    @eval('$check=' . 'is_' . $setterNeedType . '($item);');
                                    if ($check === false) {
                                        return $response->json(['data' => "repeated 参数类型错误！"]);
                                    }
                                    $setterData[] = $item;
                                } else {
                                    //对象类型
                                    $setterData[] = new $setterNeedType($item);
                                }
                            }
                        }
                    } else {
                        //optional 对象类型
                        $setterData = new $setterNeedType($value);
                    }
                }
                if (!empty($setterData)) {
                    $requestMessage->$actionTrance($setterData);
                }
            }
            $client = new GrpcTestClient('0.0.0.0:' . env('GRPC_LISTEN_PORT'), [
                'credentials' => null,
                'timeout' => 30
            ]);
            [$reply, $status, $ret] = $client->sendTest($uri, $requestMessage, $responseProto);
            if ($reply instanceof Message) {
                $reply = json_decode($reply->serializeToJsonString(), true);
            } elseif (is_string($reply)) {
                $reply = json_decode($reply, true);
            }
            $result['data'] = $reply;
            $ret->data = $reply;
            $result['ret'] = $ret;
            return $response->json($result);
        } else {
            $factory = $this->container->get(DispatcherFactory::class);
            $router = $factory->getRouter('grpc');
            [$staticRouters, $variableRouters] = $router->getData();
            $routeHtml = '<option value="">请选择</option>';
            foreach ($staticRouters as $method => $item) {
                /** @var  Handler $route */
                foreach ($item as $route) {
                    if (is_string($route->callback)) {
                        if (strpos($route->callback, '@')) {
                            $callbackArr = explode("@", $route->callback);
                        } elseif (strpos($route->callback, '::')) {
                            $callbackArr = explode("::", $route->callback);
                        } else {
                            continue;
                        }
                    } else {
                        $callbackArr = $route->callback;
                    }
                    $reflectionClass = new \ReflectionClass($callbackArr[0]);
                    if (!$reflectionClass->hasMethod($callbackArr[1])) {
                        continue;
                    }
                    $reflectionMethod = $reflectionClass->getMethod($callbackArr[1]);
                    $responseClass = $reflectionMethod->getReturnType();
                    if ($responseClass === null) {
                        continue;
                    }
                    //proto request对象
                    $reflectionParameters = $reflectionMethod->getParameters();
                    foreach ($reflectionParameters as $parameter) {
                        $requestClass = $parameter->getClass();
                    }
                    //处理对象属性
                    $propertys = $this->getPropertyByAnnotation($requestClass->name);
                    //$propertys = $this->getPropertyByRef($requestClass);
                    $propertysJson = json_encode($propertys);
                    //拼接html
                    $option = "<option data-method='%s' data-request='%s' data-response='%s' data-params='%s' value='%s'>%s</option>";
                    $optionHtml = sprintf(
                        $option,
                        $method,
                        $requestClass->name,
                        $responseClass->getName(),
                        $propertysJson,
                        $route->route . '|' . $callbackArr[0] . '|' . $callbackArr[1],
                        '【' . $method . '】' . $route->route
                    );
                    $routeHtml .= $optionHtml;
                }
            }
            $html = file_get_contents(BASE_PATH . '/vendor/swddi/grpc-helper/src/index.html');
            $html = str_replace('{$routeHtml}', $routeHtml, $html);
            return $response->withAddedHeader('content-type', 'text/html; charset=utf8')
                ->withBody(new SwooleStream((string)$html));
        }
    }

    /**
     * @param $values
     * @return array|mixed
     */
    private function formatArr($values)
    {
        if (empty($values)) {
            return [];
        }
        if (!is_array($values)) {
            return $values;
        }
        $data = [];
        foreach ($values as $key => $value) {
            if (empty($value)) {
                unset($data[$key]);
                continue;
            }
            if (is_array($value) && empty($value[0])) {
                unset($data[$key]);
                continue;
            }
            switch ($value) {
                case 'true':
                    $data[$key] = true;
                    break;
                case "false":
                    $data[$key] = false;
                    break;
                case "null":
                    $data[$key] = null;
                    break;
                case is_array($value):
                    if (isset($value[0])) {
                        $v = $this->formatArr($value);
                        if (!empty($v)) {
                            $data[$key] = $v;
                        } else {
                            unset($data[$key]);
                        }
                    } else {
                        $data[$key] = $value;
                    }
                    break;
                default:
                    @eval('$value=' . $value . ';');
                    if (!empty($value)) {
                        $data[$key] = $value;
                    } else {
                        unset($data[$key]);
                    }
                    break;

            }
        }
        return $data;
    }


    /**
     * @param $requestClass
     * @return array
     * @throws \ReflectionException
     */
    private function getPropertyByAnnotation($requestClass): array
    {
        $methodDocument = $this->getMethodDocument($requestClass, '__construct');
        foreach ($methodDocument as $item) {
            if (strpos($item, '@type') === false) {
                continue;
            }
            $lineArr = array_values(array_filter(explode(' ', $item)));
            $lineArr = array_values($lineArr);
            $name = str_replace('$', '', $lineArr[3]);
            //optional 普通数据
            if (in_array($lineArr[2], ['int', 'string', 'bool']) || $lineArr[2] === 'int|string') {
                $properties[] = $this->formatProperty($name, $name, 'optional', $lineArr[2]);
                continue;
            }
            //optional 对象
            $typeArr = explode('|', $lineArr[2]);
            if (count($typeArr) == 1) {
                $repeatedClass = new ReflectionClass($typeArr[0]);
                $remark = [];
                foreach ($repeatedClass->getProperties() as $property) {
                    $remark[] = $property->name;
                }
                $properties[] = $this->formatProperty($name, $repeatedClass->name, 'optional', json_encode($remark, 320));
                continue;
            }
            //map
            $typeArr = explode('|', $lineArr[2]);
            if ($lineArr[2] == 'array|\Google\Protobuf\Internal\MapField') {
                $methodDocument1 = $this->getMethodDocument($requestClass, $this->actionTrance($name));
                $between = str_replace(' ', '', Str::between($methodDocument1[3], '<code>map<', '> ' . $name));
                $remark = explode(',', $between);
                $properties[] = $this->formatProperty($name, $typeArr[1], 'map', $remark);
                continue;
            }
            //repeated
            if ($typeArr[1] == '\Google\Protobuf\Internal\RepeatedField') {
                $className = str_replace('[]', '', $typeArr[0]);
                //普通数据
                if (in_array($className, ['int', 'string', 'bool'])) {
                    $properties[] = $this->formatProperty($name, $name, 'repeated', $className);
                } else {
                    // 对象
                    $repeatedClass = new \ReflectionClass($className);
                    $remark = [];
                    foreach ($repeatedClass->getProperties() as $property) {
                        $remark[] = $property->name;
                    }
                    $properties[] = $this->formatProperty($name, $repeatedClass->name, 'repeated', json_encode($remark, 320));
                }
            }
        }
        return $properties;
    }

    /**
     * @param ReflectionClass $requestClass
     * @return array
     * @deprecated
     */
    private function getPropertyByRef(ReflectionClass $requestClass): array
    {
        $properties = [];
        foreach ($requestClass->getProperties() as $property) {
            $docComment = $property->getDocComment();
            //识别repeated字段
            if (strpos($docComment, '<code>repeated') !== false) {
                $properties[] = $this->formatProperty($property->name, $property->name, 'repeated');
            }
            if (strpos($docComment, '<code>optional') !== false) {
                $properties[] = $this->formatProperty($property->name, $property->name, 'optional');
            }
            if (strpos($docComment, '<code>map') !== false) {
                $properties[] = $this->formatProperty($property->name, $property->name, 'map');
            }
        }
        return $properties;
    }

    /**
     * @param $name
     * @param string $type
     * @param $showName
     * @param $remark
     * @return array
     */
    private function formatProperty($name, $showName, string $type = 'optional', $remark = ''): array
    {
        $data = [
            'name' => $name,
            'show_name' => $showName,
            'type' => $type,
            'remark' => $remark,
        ];
        if ($type == 'map') {
            $data['map_type'] = $remark;
        }
        return $data;
    }

    /**
     * @param $name
     * @return string
     */
    private function actionTrance($name): string
    {
        $explode = explode('_', $name);
        $action = 'set';
        foreach ($explode as $item) {
            $action .= ucfirst($item);
        }
        return $action;
    }

    /**
     * @param $requestObj
     * @param $method
     * @return array
     * @throws \ReflectionException
     */
    private function getSetterNeedType($requestObj, $method)
    {
        $msg = $type = '';
        $methodDoc = $this->getMethodDocument($requestObj, $method);
        foreach ($methodDoc as $item) {
            if (strpos($item, '@param') === false) {
                if (strpos($item, '<code>map<') === false) {
                    continue;
                }
                //如果是map，禁止第二个参数传对象
                $cutStr = Str::between(Str::between($item, ',', '>', ), ' ', '>');
                $explode = explode('.', $cutStr);
                if (count($explode) > 1) {
                    $msg = "not support class map! $cutStr";
                    break;
                }
            }
            $lineArr = array_values(array_filter(explode(' ', $item)));
            $typeArr = explode('|', $lineArr[2]);
            if (count($typeArr) == 1) {
                $type = $lineArr[2];
            } else {
                $type = $typeArr[0];
            }
        }
        return [$type, $msg];
    }

    /**
     * 获取指定方法的doc
     *
     * @param $class
     * @param $method
     * @return array
     * @throws \ReflectionException
     */
    private function getMethodDocument($class, $method): array
    {
        if (!empty($this->refmap[$class])) {
            $reflectionClass = $this->refmap[$class];
        } else {
            $reflectionClass = new \ReflectionClass($class);
        }
        $reflectionMethod = $reflectionClass->getMethod($method);
        $docComment = $reflectionMethod->getDocComment();
        return explode(PHP_EOL, $docComment);
    }

    /**
     * @param $begin
     * @param $end
     * @param $str
     * @return string
     */
    private function cutStr($begin, $end, $str): string
    {
        $indexA = strpos($str, $begin) + strlen($begin);
        $stra = substr($str, $indexA);
        $lastStr = substr($str, strlen($stra));
        $indexB = strpos($lastStr, $end);
        return substr($lastStr, 0, $indexB);
    }
}
